<?php
include 'project_1.php';
if(isset($_POST['action']))

{ 
$sql= 'insert into traveling(nama_kota,nama_daerah,penginapan) values(?,?,?)';

$result=$db->prepare($sql);
$result->bindValue(1, $_POST['nama_kota'],PDO::PARAM_STR);
$result->bindValue(2, $_POST['nama_daerah'],PDO::PARAM_STR);    
$result->bindValue(3, $_POST['penginapan'],PDO::PARAM_STR);

$result->execute();
header('location: pembayaran.php');
}
// if($result->execute())
// {
//     $data=$db->prepare('select * from traveling');

//     $data->execute();

//     $data_siswa=$data->fetchAll();

// }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- css for font awsome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <!-- css undang -->
    <link rel="stylesheet" href="/css/bayar.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col">   
    <!-- allert Masage -->
    <table id="bg1" class="table table-border table-primary">
        <thead>
            <tr class="table-active">
                <th scope="col">nama kota</th>
                <th scope="col">nama daerah</th>
                <!-- <th scope="col">total harga</th> -->
                <th scope="col">Penginapan</th>
                <th scope="col">Rubah</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data_siswa as $key):?>
                    <td><?php echo $key['nama_kota'];?></td>
                    <td><?php echo $key['nama_daerah'];?></td>
                    <!-- <td><php echo $key['total_harga'];?></td> -->
                    <td><?php echo $key['penginapan'];?></td>
                    <td><a href="delete.php?id=<?php echo $key['id']; ?>"><i class="fas fa-trash-alt"></i></a> | <a href="edit.php?id=<?php echo $key['id']; ?>"><i class="fas fa-user-edit"></i></a></td>
                </tr>
                
                <?php endforeach; ?>
        </tbody>
    </table>
                        
       </div>
      </div>
     </div>
</body>
</html>
